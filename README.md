# GPT Survey Data



## Getting started

Repository for Chat GPT survey data of political science professors by Phillip Ardoin. The treatment and outcomes (AI_Allowed and Trust) respectively are created in a semi-synthetic manner by me to allow for evaluation of causal methods.

Citation for the non-synthetic source data:

@data{DVN/U5IQBE_2024,
author = {Ardoin, Phillip},
publisher = {Harvard Dataverse},
title = {{Replication Data for Fear and Loathing, Chat GPT in the Political Science Classroom}},
UNF = {UNF:6:Jy+XxDDQI7nWwhwZvDpXEQ==},
year = {2024},
version = {V2},
doi = {10.7910/DVN/U5IQBE},
url = {https://doi.org/10.7910/DVN/U5IQBE}
}